(function () {
    'use strict';
    var CC = function (param) {
        var cc = {};
        cc.dom = document.querySelectorAll(param);
        cc.exports = {
            nodeList: function () {
                return cc.dom;
            },
            each: function (callback) {
                var i = 0;
                for (i = 0; i < cc.dom.length; i += 1) {
                    callback(i, cc.dom[i]);
                }
            },
            click: function (handler) {
                cc.exports.each(function (index, element) {
                    element.onclick = handler;
                });
            }
        };
        return cc.exports;
    };
    window.CC = CC;
}());

var YEAR = [1986, 1989, 1990, 1991, 1995, 1996, 1999, 2001, 2005, 2009, 2014, 2016],
    ANSWER = [1, 0, 2, 1, 2, 0, 1, 1, 2, 0, 1, 0],
    LANG = [
        '恭喜你，全都答对了！',
        '猜对这么多，你暴露年龄了！',
        '诶呦，还不错！',
        '看来这些题对你有些难...',
        '你是不是就没看过春晚？'
    ],
    nStep = 0,
    nQuest = 0,
    aAnswer = [],
    nPoint = 0;

var btnEnter = window.CC('.btn-enter'),
    btnNext = window.CC('.btn-next'),
    btnSubmit = window.CC('.btn-submit'),
    btnAgain = window.CC('.btn-again'),
    btnShare = window.CC('.btn-share'),
    step = window.CC('.step'),
    layerCenter = window.CC('.layer-center'),
    layerBottom = window.CC('.layer-bottom'),
    answerUl = window.CC('.answer'),
    answerLi = window.CC('.answer li'),
    musicPlayer = window.CC('audio'),
    btnNotice = window.CC('.notice div'),
    pointResult = window.CC('.result span'),
    textResult = window.CC('.desc');

var music = function (play) {
        if (play) {
            musicPlayer.nodeList()[0].play();
        } else {
            musicPlayer.nodeList()[0].pause();
        }
    },
    goStep = function () {
        step.each(function (index, element) {
            element.style.display = (index === nStep) ? 'block' : 'none';
        });
    },
    nextStep = function () {
        nStep += 1;
        goStep();
        layerCenter.nodeList()[0].classList.remove('cover');
        layerBottom.nodeList()[0].style.backgroundImage = 'url(assets/bg_skit/' + YEAR[nQuest] + '.jpg)';
        music(true);
    },
    nextQuest = function () {
        nStep += 1;
        goStep();
        nQuest += 1;
        layerBottom.nodeList()[0].style.backgroundImage = 'url(assets/bg_skit/' + YEAR[nQuest] + '.jpg)';
        musicPlayer.nodeList()[0].src = 'assets/bgm/' + (nQuest + 1) + '.mp3';
        music(true);
    },
    onSelect = function () {
        var here = answerUl.nodeList()[nQuest],
            siblings = here.querySelectorAll('li'),
            index = 0,
            i = 0;
        for (i = 0; i < siblings.length; i += 1) {
            if (siblings[i] === this) {
                siblings[i].classList.add('selected');
                aAnswer[nQuest] = i;
            } else {
                siblings[i].classList.remove('selected');
            }
        }
    },
    onSubmit = function () {
        var i = 0;
        for (i = 0; i < aAnswer.length; i += 1) {
            nPoint += (aAnswer[i] === ANSWER[i]) ? 1 : 0;
        }
        pointResult.nodeList()[0].innerHTML = nPoint;
        if (nPoint === 12) {
            textResult.nodeList()[0].innerHTML = LANG[0];
        } else if (nPoint >= 9 && nPoint < 12) {
            textResult.nodeList()[0].innerHTML = LANG[1];
        } else if (nPoint >= 5 && nPoint < 9) {
            textResult.nodeList()[0].innerHTML = LANG[2];
        } else if (nPoint >= 1 && nPoint < 5) {
            textResult.nodeList()[0].innerHTML = LANG[3];
        } else if (nPoint === 0) {
            textResult.nodeList()[0].innerHTML = LANG[4];
        }
        window.console.log(aAnswer);
        window.console.log(nPoint);
        nStep += 1;
        goStep();
        layerCenter.nodeList()[0].classList.add('cover');
        music(false);
    },
    onAgain = function () {
        window.location.reload();
    },
    onShare = function () {
        window.alert('点击右上角分享给微信好友～');
    },
    showNotice = function () {
        var target = this.querySelectorAll('p')[0];
        target.classList.add('show');
        setTimeout(function () {
            target.classList.remove('show');
        }, 1000);
    };

btnEnter.click(nextStep);
btnNext.click(nextQuest);
btnSubmit.click(onSubmit);
btnAgain.click(onAgain);
btnShare.click(onShare);
answerLi.click(onSelect);
btnNotice.click(showNotice);
goStep();
